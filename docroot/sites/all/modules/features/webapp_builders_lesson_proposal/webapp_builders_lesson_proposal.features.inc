<?php
/**
 * @file
 * webapp_builders_lesson_proposal.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function webapp_builders_lesson_proposal_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function webapp_builders_lesson_proposal_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function webapp_builders_lesson_proposal_flag_default_flags() {
  $flags = array();
  // Exported flag: "Interested in Lesson".
  $flags['interested_in_lesson'] = array(
    'entity_type' => 'node',
    'title' => 'Interested in Lesson',
    'global' => 0,
    'types' => array(
      0 => 'lesson_proposal',
    ),
    'flag_short' => '☐ I am interested in the lesson',
    'flag_long' => '',
    'flag_message' => 'Awesome!',
    'unflag_short' => '☑ I am interested in the lesson',
    'unflag_long' => '',
    'unflag_message' => 'Not so awesome :(',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'webapp_builders_lesson_proposal',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Potential Presenter".
  $flags['potential_presenter'] = array(
    'entity_type' => 'node',
    'title' => 'Potential Presenter',
    'global' => 0,
    'types' => array(
      0 => 'lesson_proposal',
    ),
    'flag_short' => '☐ I can present on topic',
    'flag_long' => '',
    'flag_message' => 'Thank you for offering to present!',
    'unflag_short' => '☑ I can present on topic',
    'unflag_long' => '',
    'unflag_message' => 'We\'re sorry you cannot present.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'webapp_builders_lesson_proposal',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function webapp_builders_lesson_proposal_node_info() {
  $items = array(
    'lesson_proposal' => array(
      'name' => t('Lesson Proposal'),
      'base' => 'node_content',
      'description' => t('Create a lesson proposal - What can you present on? Or what would you like a presentation on?'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
