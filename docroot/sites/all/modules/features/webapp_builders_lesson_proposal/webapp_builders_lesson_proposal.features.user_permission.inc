<?php
/**
 * @file
 * webapp_builders_lesson_proposal.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function webapp_builders_lesson_proposal_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_proposal_presentation_date'.
  $permissions['create field_proposal_presentation_date'] = array(
    'name' => 'create field_proposal_presentation_date',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_proposal_presentation_date'.
  $permissions['edit field_proposal_presentation_date'] = array(
    'name' => 'edit field_proposal_presentation_date',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_proposal_presentation_date'.
  $permissions['edit own field_proposal_presentation_date'] = array(
    'name' => 'edit own field_proposal_presentation_date',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_proposal_presentation_date'.
  $permissions['view field_proposal_presentation_date'] = array(
    'name' => 'view field_proposal_presentation_date',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_proposal_presentation_date'.
  $permissions['view own field_proposal_presentation_date'] = array(
    'name' => 'view own field_proposal_presentation_date',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
