<?php
/**
 * @file
 * webapp_builders_lesson_proposal.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function webapp_builders_lesson_proposal_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_view_panel_context';
  $handler->task = 'node_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'entity_bundle:node',
          'settings' => array(
            'type' => array(
              'lesson_proposal' => 'lesson_proposal',
            ),
          ),
          'context' => 'argument_entity_id:node_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left_above' => NULL,
      'right_above' => NULL,
      'middle' => NULL,
      'left_below' => NULL,
      'right_below' => NULL,
      'bottom' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '32c7fba4-2551-4a4e-9432-d465251efbdd';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-1791944e-5dbd-4b7f-982f-fa9c4833e0a3';
    $pane->panel = 'bottom';
    $pane->type = 'block';
    $pane->subtype = 'disqus-disqus_comments';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '1791944e-5dbd-4b7f-982f-fa9c4833e0a3';
    $display->content['new-1791944e-5dbd-4b7f-982f-fa9c4833e0a3'] = $pane;
    $display->panels['bottom'][0] = 'new-1791944e-5dbd-4b7f-982f-fa9c4833e0a3';
    $pane = new stdClass();
    $pane->pid = 'new-d9583d36-6bdd-4ccd-9602-f928dbecfb82';
    $pane->panel = 'left';
    $pane->type = 'node_links';
    $pane->subtype = 'node_links';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'build_mode' => 'full',
      'identifier' => '',
      'link' => 1,
      'context' => 'argument_entity_id:node_1',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd9583d36-6bdd-4ccd-9602-f928dbecfb82';
    $display->content['new-d9583d36-6bdd-4ccd-9602-f928dbecfb82'] = $pane;
    $display->panels['left'][0] = 'new-d9583d36-6bdd-4ccd-9602-f928dbecfb82';
    $pane = new stdClass();
    $pane->pid = 'new-127a13ef-3fc0-43b7-b06c-a95bb425f7b8';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_proposal_submitted_by';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '127a13ef-3fc0-43b7-b06c-a95bb425f7b8';
    $display->content['new-127a13ef-3fc0-43b7-b06c-a95bb425f7b8'] = $pane;
    $display->panels['right'][0] = 'new-127a13ef-3fc0-43b7-b06c-a95bb425f7b8';
    $pane = new stdClass();
    $pane->pid = 'new-320489a9-43ea-430d-be53-1fc9788182a6';
    $pane->panel = 'right';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_proposal_contact_email';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'email_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '320489a9-43ea-430d-be53-1fc9788182a6';
    $display->content['new-320489a9-43ea-430d-be53-1fc9788182a6'] = $pane;
    $display->panels['right'][1] = 'new-320489a9-43ea-430d-be53-1fc9788182a6';
    $pane = new stdClass();
    $pane->pid = 'new-cfc91d64-5d4f-4c2f-98ab-dcc917450e80';
    $pane->panel = 'top';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:body';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'hidden',
      'formatter' => 'text_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'cfc91d64-5d4f-4c2f-98ab-dcc917450e80';
    $display->content['new-cfc91d64-5d4f-4c2f-98ab-dcc917450e80'] = $pane;
    $display->panels['top'][0] = 'new-cfc91d64-5d4f-4c2f-98ab-dcc917450e80';
    $pane = new stdClass();
    $pane->pid = 'new-ca8498f4-51b4-48d7-92b6-eb6da8f42854';
    $pane->panel = 'top';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_proposal_presentation_date';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'inline',
      'formatter' => 'date_default',
      'delta_limit' => 0,
      'delta_offset' => '0',
      'delta_reversed' => FALSE,
      'formatter_settings' => array(
        'format_type' => 'medium',
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_to' => '',
        'fromto' => 'both',
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 0,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'ca8498f4-51b4-48d7-92b6-eb6da8f42854';
    $display->content['new-ca8498f4-51b4-48d7-92b6-eb6da8f42854'] = $pane;
    $display->panels['top'][1] = 'new-ca8498f4-51b4-48d7-92b6-eb6da8f42854';
    $pane = new stdClass();
    $pane->pid = 'new-e60f3c55-a62c-40cf-8306-deedcca9c489';
    $pane->panel = 'top';
    $pane->type = 'entity_field';
    $pane->subtype = 'node:field_presenters';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => 'title',
      'formatter' => 'entityreference_label',
      'delta_limit' => '0',
      'delta_offset' => '0',
      'delta_reversed' => 0,
      'formatter_settings' => array(
        'link' => 0,
      ),
      'context' => 'argument_entity_id:node_1',
      'override_title' => 1,
      'override_title_text' => 'Presenters',
      'override_title_heading' => 'h4',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'e60f3c55-a62c-40cf-8306-deedcca9c489';
    $display->content['new-e60f3c55-a62c-40cf-8306-deedcca9c489'] = $pane;
    $display->panels['top'][2] = 'new-e60f3c55-a62c-40cf-8306-deedcca9c489';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['node_view_panel_context'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function webapp_builders_lesson_proposal_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'lessons';
  $page->task = 'page';
  $page->admin_title = 'Lessons';
  $page->admin_description = '';
  $page->path = 'lessons';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_lessons__panel_context_cd8e587f-89ae-4894-ae7a-a1ffd2b01944';
  $handler->task = 'page';
  $handler->subtask = 'lessons';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'twocol_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'middle' => NULL,
    ),
    'style' => 'naked',
  );
  $display->cache = array();
  $display->title = 'Welcome to the <em>Webapp Builders</em> group!';
  $display->uuid = 'a3031c6e-0889-4531-9845-d6f1e1f849d1';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-443cf1f5-8ddd-4831-88fc-1c4379578c53';
    $pane->panel = 'top';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '1',
      'links' => 1,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 1,
      'override_title_text' => '',
      'override_title_heading' => 'span',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
      'style' => 'default',
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '443cf1f5-8ddd-4831-88fc-1c4379578c53';
    $display->content['new-443cf1f5-8ddd-4831-88fc-1c4379578c53'] = $pane;
    $display->panels['top'][0] = 'new-443cf1f5-8ddd-4831-88fc-1c4379578c53';
    $pane = new stdClass();
    $pane->pid = 'new-dff9ed8b-e5de-43d9-94de-d39fb18f25ad';
    $pane->panel = 'top';
    $pane->type = 'views_panes';
    $pane->subtype = 'lessons-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'dff9ed8b-e5de-43d9-94de-d39fb18f25ad';
    $display->content['new-dff9ed8b-e5de-43d9-94de-d39fb18f25ad'] = $pane;
    $display->panels['top'][1] = 'new-dff9ed8b-e5de-43d9-94de-d39fb18f25ad';
    $pane = new stdClass();
    $pane->pid = 'new-1aecbc13-5ef5-456f-a8ba-74760c9ed417';
    $pane->panel = 'top';
    $pane->type = 'views_panes';
    $pane->subtype = 'lessons-panel_pane_4';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '1aecbc13-5ef5-456f-a8ba-74760c9ed417';
    $display->content['new-1aecbc13-5ef5-456f-a8ba-74760c9ed417'] = $pane;
    $display->panels['top'][2] = 'new-1aecbc13-5ef5-456f-a8ba-74760c9ed417';
    $pane = new stdClass();
    $pane->pid = 'new-ff383ae8-589d-4129-933f-e41a1717a2a1';
    $pane->panel = 'top';
    $pane->type = 'views_panes';
    $pane->subtype = 'lessons-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 2,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'ff383ae8-589d-4129-933f-e41a1717a2a1';
    $display->content['new-ff383ae8-589d-4129-933f-e41a1717a2a1'] = $pane;
    $display->panels['top'][3] = 'new-ff383ae8-589d-4129-933f-e41a1717a2a1';
    $pane = new stdClass();
    $pane->pid = 'new-c5b49dbf-9f7f-45f3-9943-9fd672341fa0';
    $pane->panel = 'top';
    $pane->type = 'views_panes';
    $pane->subtype = 'lessons-panel_pane_3';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array();
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = 'c5b49dbf-9f7f-45f3-9943-9fd672341fa0';
    $display->content['new-c5b49dbf-9f7f-45f3-9943-9fd672341fa0'] = $pane;
    $display->panels['top'][4] = 'new-c5b49dbf-9f7f-45f3-9943-9fd672341fa0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-443cf1f5-8ddd-4831-88fc-1c4379578c53';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['lessons'] = $page;

  return $pages;

}
