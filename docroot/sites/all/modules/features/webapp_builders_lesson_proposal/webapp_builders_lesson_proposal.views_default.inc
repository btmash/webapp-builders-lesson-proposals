<?php
/**
 * @file
 * webapp_builders_lesson_proposal.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function webapp_builders_lesson_proposal_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'flagged_presenters';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Flagged Presenters';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Flags: potential_presenter */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'potential_presenter';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Relationship: Flags: User */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'flagging';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['relationship'] = 'flag_content_rel';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['overwrite_anonymous'] = TRUE;
  $handler->display->display_options['fields']['name']['anonymous_text'] = 'N/A';
  /* Contextual filter: Content: Nid */
  $handler->display->display_options['arguments']['nid']['id'] = 'nid';
  $handler->display->display_options['arguments']['nid']['table'] = 'node';
  $handler->display->display_options['arguments']['nid']['field'] = 'nid';
  $handler->display->display_options['arguments']['nid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['nid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['nid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['nid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['nid']['summary_options']['items_per_page'] = '25';
  $export['flagged_presenters'] = $view;

  $view = new view();
  $view->name = 'lessons';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'lessons';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Upcoming Lessons';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'There are no proposed upcoming lessons with a date.';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Session Name';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Presentation Date */
  $handler->display->display_options['fields']['field_proposal_presentation_date']['id'] = 'field_proposal_presentation_date';
  $handler->display->display_options['fields']['field_proposal_presentation_date']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['fields']['field_proposal_presentation_date']['field'] = 'field_proposal_presentation_date';
  $handler->display->display_options['fields']['field_proposal_presentation_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  /* Field: Content: Presenters */
  $handler->display->display_options['fields']['field_presenters']['id'] = 'field_presenters';
  $handler->display->display_options['fields']['field_presenters']['table'] = 'field_data_field_presenters';
  $handler->display->display_options['fields']['field_presenters']['field'] = 'field_presenters';
  $handler->display->display_options['fields']['field_presenters']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_presenters']['delta_offset'] = '0';
  /* Sort criterion: Flags: Flag counter */
  $handler->display->display_options['sorts']['count']['id'] = 'count';
  $handler->display->display_options['sorts']['count']['table'] = 'flag_counts';
  $handler->display->display_options['sorts']['count']['field'] = 'count';
  $handler->display->display_options['sorts']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['sorts']['count']['order'] = 'DESC';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'lesson_proposal' => 'lesson_proposal',
  );
  /* Filter criterion: Content: Presentation Date (field_proposal_presentation_date) */
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['id'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['field'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['default_date'] = 'now';

  /* Display: Upcoming Lessons */
  $handler = $view->new_display('panel_pane', 'Upcoming Lessons', 'panel_pane_1');
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Presentation Date (field_proposal_presentation_date) */
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['id'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['field'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['order'] = 'DESC';
  $handler->display->display_options['allow']['use_pager'] = 0;
  $handler->display->display_options['allow']['items_per_page'] = 0;
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 0;
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 'title_override';
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;

  /* Display: Proposed Lessons */
  $handler = $view->new_display('panel_pane', 'Proposed Lessons', 'panel_pane_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Proposed Lessons';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Flags: potential_presenter */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['label'] = 'Potential Presenter';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'potential_presenter';
  /* Relationship: Flags: interested_in_lesson */
  $handler->display->display_options['relationships']['flag_content_rel_1']['id'] = 'flag_content_rel_1';
  $handler->display->display_options['relationships']['flag_content_rel_1']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel_1']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel_1']['label'] = 'Interested in Lesson';
  $handler->display->display_options['relationships']['flag_content_rel_1']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel_1']['flag'] = 'interested_in_lesson';
  /* Relationship: Flags: interested_in_lesson counter */
  $handler->display->display_options['relationships']['flag_count_rel']['id'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_count_rel']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_count_rel']['flag'] = 'interested_in_lesson';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Session Name';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['fields']['count']['label'] = 'Interested in lesson';
  $handler->display->display_options['fields']['count']['exclude'] = TRUE;
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops']['id'] = 'ops';
  $handler->display->display_options['fields']['ops']['table'] = 'flagging';
  $handler->display->display_options['fields']['ops']['field'] = 'ops';
  $handler->display->display_options['fields']['ops']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['fields']['ops']['label'] = 'Interested in Presenting';
  /* Field: Flags: Flag link */
  $handler->display->display_options['fields']['ops_1']['id'] = 'ops_1';
  $handler->display->display_options['fields']['ops_1']['table'] = 'flagging';
  $handler->display->display_options['fields']['ops_1']['field'] = 'ops';
  $handler->display->display_options['fields']['ops_1']['relationship'] = 'flag_content_rel_1';
  $handler->display->display_options['fields']['ops_1']['label'] = 'Interested in Lesson';
  $handler->display->display_options['fields']['ops_1']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['ops_1']['alter']['text'] = '[ops_1] ([count])';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'lesson_proposal' => 'lesson_proposal',
  );
  /* Filter criterion: Presentation Date */
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['id'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['field'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['ui_name'] = 'Presentation Date';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['operator'] = 'empty';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['default_date'] = 'now -1 day';

  /* Display: Under Review */
  $handler = $view->new_display('panel_pane', 'Under Review', 'panel_pane_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Under Review';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Flags: interested_in_lesson counter */
  $handler->display->display_options['relationships']['flag_count_rel']['id'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_count_rel']['field'] = 'flag_count_rel';
  $handler->display->display_options['relationships']['flag_count_rel']['label'] = 'Interested';
  $handler->display->display_options['relationships']['flag_count_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_count_rel']['flag'] = 'interested_in_lesson';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Session Name';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Nid */
  $handler->display->display_options['fields']['nid']['id'] = 'nid';
  $handler->display->display_options['fields']['nid']['table'] = 'node';
  $handler->display->display_options['fields']['nid']['field'] = 'nid';
  $handler->display->display_options['fields']['nid']['label'] = '';
  $handler->display->display_options['fields']['nid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['nid']['element_label_colon'] = FALSE;
  /* Field: Global: View */
  $handler->display->display_options['fields']['view']['id'] = 'view';
  $handler->display->display_options['fields']['view']['table'] = 'views';
  $handler->display->display_options['fields']['view']['field'] = 'view';
  $handler->display->display_options['fields']['view']['label'] = 'Interested Presenters';
  $handler->display->display_options['fields']['view']['view'] = 'flagged_presenters';
  $handler->display->display_options['fields']['view']['arguments'] = '[%nid]';
  /* Field: Flags: Flag counter */
  $handler->display->display_options['fields']['count']['id'] = 'count';
  $handler->display->display_options['fields']['count']['table'] = 'flag_counts';
  $handler->display->display_options['fields']['count']['field'] = 'count';
  $handler->display->display_options['fields']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['fields']['count']['label'] = 'Total interested in Lesson';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Flags: Flag counter */
  $handler->display->display_options['sorts']['count']['id'] = 'count';
  $handler->display->display_options['sorts']['count']['table'] = 'flag_counts';
  $handler->display->display_options['sorts']['count']['field'] = 'count';
  $handler->display->display_options['sorts']['count']['relationship'] = 'flag_count_rel';
  $handler->display->display_options['sorts']['count']['order'] = 'DESC';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'lesson_proposal' => 'lesson_proposal',
  );
  /* Filter criterion: Presentation Date */
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['id'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['field'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['ui_name'] = 'Presentation Date';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['operator'] = 'empty';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['default_date'] = 'now -1 day';

  /* Display: Past Lessons */
  $handler = $view->new_display('panel_pane', 'Past Lessons', 'panel_pane_4');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Past Lessons';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Presentation Date (field_proposal_presentation_date) */
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['id'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['field'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['sorts']['field_proposal_presentation_date_value']['order'] = 'DESC';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'lesson_proposal' => 'lesson_proposal',
  );
  /* Filter criterion: Content: Presentation Date (field_proposal_presentation_date) */
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['id'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['field'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['operator'] = '<=';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value']['default_date'] = 'now';
  /* Filter criterion: Content: Presentation Date (field_proposal_presentation_date) */
  $handler->display->display_options['filters']['field_proposal_presentation_date_value_1']['id'] = 'field_proposal_presentation_date_value_1';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value_1']['table'] = 'field_data_field_proposal_presentation_date';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value_1']['field'] = 'field_proposal_presentation_date_value';
  $handler->display->display_options['filters']['field_proposal_presentation_date_value_1']['operator'] = 'not empty';
  $export['lessons'] = $view;

  return $export;
}
