<?php
/**
 * @file
 * webapp_builders_lesson_proposal.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function webapp_builders_lesson_proposal_default_rules_configuration() {
  $items = array();
  $items['rules_new_user_registered_notification'] = entity_import('rules_config', '{ "rules_new_user_registered_notification" : {
      "LABEL" : "New user Registered Notification",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_insert" : [] },
      "DO" : [
        { "mail" : {
            "to" : "btmash+webapps@gmail.com, getkobi@gmail.com",
            "subject" : "[WebApp Builders] - New User Registered on site",
            "message" : "Hello,\\r\\n\\r\\nA new user ([account:name]) has registered - please review on webapps site ([site:url]) for more information.\\r\\n\\r\\nCheers,\\r\\nWebApp Builders Team",
            "from" : "no-reply@webapps.btmash.com",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_send_email_on_new_proposal'] = entity_import('rules_config', '{ "rules_send_email_on_new_proposal" : {
      "LABEL" : "Send email on new proposal",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "node_insert--lesson_proposal" : { "bundle" : "lesson_proposal" } },
      "DO" : [
        { "mail" : {
            "to" : "btmash@gmail.com, getkobi@gmail.com",
            "subject" : "[Webapp Builders] - New lesson proposal",
            "message" : "Dear Webapp Builder admin,\\r\\n\\r\\nA new lesson proposal has been created by [node:field_proposal_submitted_by] ([node:field_proposal_contact_email]) at [node:url]. Please log in to view.\\r\\n\\r\\nSincerely,\\r\\nWebapp builders team",
            "from" : "no-reply@btmash.com",
            "language" : [ "" ]
          }
        },
        { "node_unpublish" : { "node" : [ "node" ] } },
        { "drupal_message" : { "message" : "Thank you for creating the lesson proposal! We will contact you for more information." } },
        { "redirect" : { "url" : "[site:url]" } }
      ]
    }
  }');
  return $items;
}
